FROM quay.io/goern/armv7hf-nodejs-fedora 

RUN git clone https://github.com/dale3h/hue-mqtt-bridge.git && \
    cd hue-mqtt-bridge && \ 
    npm install

ADD config.json ${HOME}/hue-mqtt-bridge/

WORKDIR ${HOME}/hue-mqtt-bridge/

CMD ./bin/hue-mqtt-bridge
