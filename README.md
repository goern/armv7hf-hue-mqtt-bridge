# Hue MQTT Bridge

This is a containerized version of https://github.com/dale3h/hue-mqtt-bridge. 

To build the container run `docker build --rm --tag armv7hf-hue-mqtt-bridge .`.

## Configuration

You must provide a configuration file `config.json`, see https://github.com/dale3h/hue-mqtt-bridge/blob/master/config.sample.json
